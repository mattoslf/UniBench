/*
 * Copyright (C) 2013 Michael Andersch <michael.andersch@mailbox.tu-berlin.de>
 *
 * This file is part of Starbench.
 *
 * Starbench is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Starbench is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Starbench.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _H_KMEANS
#define _H_KMEANS

#include <assert.h>

double** starss_kmeans(int, double**, int, int, int, double, int*);

double** kmeans_seq(int, double**, int, int, int, double, int*);

double** file_read(int, char*, int*, int*);

float  wtime(void);

extern int find_nearest_cluster(int numClusters, int numCoords, double *object, double **clusters);

extern double euclid_dist_2(int numdims, double *coord1, double *coord2);

#endif
