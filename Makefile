PLATFORM_MK=./common/c.mk
include $(PLATFORM_MK)

BENCH_MK=$(BENCH_DIR)/../Makefile
include $(BENCH_MK)

SRC_MK=$(BENCH_DIR)/src/Makefile
include $(SRC_MK)

LIMIT_FLAG=""


$(BENCH_DIR)/build/$(BENCH_NAME):
	make compile

$(BENCH_DIR)/build/seq/$(BENCH_NAME):
	make compile

$(BENCH_DIR)/build/gomp/$(BENCH_NAME):
	make compile

$(BENCH_DIR)/build/iomp/$(BENCH_NAME):
	make compile

$(BENCH_DIR)/build/mtsp/$(BENCH_NAME):
	make compile

$(BENCH_DIR)/build/tioga/$(BENCH_NAME):
	make compile

$(BENCH_DIR)/build:
	mkdir $(BENCH_DIR)/build

$(BENCH_DIR)/log:
	mkdir $(BENCH_DIR)/log

$(BENCH_DIR)/output:
	mkdir $(BENCH_DIR)/output

$(BENCH_DIR)/build/mtsp:
	mkdir $(BENCH_DIR)/build/mtsp

$(BENCH_DIR)/build/iomp:
	mkdir $(BENCH_DIR)/build/iomp

$(BENCH_DIR)/build/gomp:
	mkdir $(BENCH_DIR)/build/gomp

$(BENCH_DIR)/build/seq:
	mkdir $(BENCH_DIR)/build/seq

$(BENCH_DIR)/build/tioga:
	mkdir $(BENCH_DIR)/build/tioga

cleanoutput:
	rm -rf $(BENCH_DIR)/output/*

cleanbin:
	rm -rf $(BENCH_DIR)/build/*

cleanlog:
	rm -rf $(BENCH_DIR)/log/*

cleanall: cleanbin cleanlog cleanoutput

compile:
ifeq ($(OWN_MAKEFILE),YES)
	cd $(BENCH_DIR)/src/build_example; \
	make compile OMP_LIB=$(OMP_LIB)
endif

ifeq ($(OMP_LIB),gomp)
	make compileGOMP
endif
ifeq ($(OMP_LIB),iomp)
	make compileIOMP
endif
ifeq ($(OMP_LIB),mtsp)
	make compileMTSP
endif
ifeq ($(OMP_LIB),tioga)
	make compileTIOGA
endif
ifeq ($(OMP_LIB),seq)
	make compileSEQ
endif
ifeq ($(OMP_LIB),none)
	make compileSimple
endif

ifeq ($(TGT_ARCH),gpu)
	make compileGPU
endif
#ifeq ($(TGT_ARCH),gpu)
#	make compileGPU
#else
#	make compileSimple
#endif

compileSimple: $(BENCH_DIR)/build $(BENCH_DIR)/log
	echo "Compiling" $(BENCH_NAME); \
	echo "\n---------------------------------------------------------" >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	date >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo "$(CC) $(COMMON_FLAGS) $(BENCH_FLAGS) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/$(BENCH_NAME)" 2>> $(BENCH_DIR)/log/$(NAME).compile; \
	$(CC) $(COMMON_FLAGS) $(BENCH_FLAGS) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/$(BENCH_NAME) 2>> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo ""

compileGPU: $(BENCH_DIR)/build $(BENCH_DIR)/log
	rm -f _kernel*.cl~
	mv _kernel* $(BENCH_DIR)/build/; \
	echo ""

compileSEQ: $(BENCH_DIR)/build $(BENCH_DIR)/log $(BENCH_DIR)/build/seq
ifeq ($(CPP_FLAG),YES)
	$(eval CC = $(CXX))
endif
	echo "Compiling" $(BENCH_NAME); \
	echo "\n---------------------------------------------------------" >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	date >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo "$(CC) $(BENCH_FLAGS) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/seq/$(BENCH_NAME)" 2>> $(BENCH_DIR)/log/$(NAME).compile; \
	$(CC) $(BENCH_FLAGS) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/seq/$(BENCH_NAME) 2>> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo ""

compileGOMP: $(BENCH_DIR)/build $(BENCH_DIR)/log $(BENCH_DIR)/build/gomp
ifeq ($(CPP_FLAG),YES)
	$(eval CC_GOMP = $(CXX_GOMP))
endif
	echo "Compiling" $(BENCH_NAME); \
	echo "\n---------------------------------------------------------" >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	date >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo "$(CC_GOMP) $(COMMON_FLAGS) $(BENCH_FLAGS) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/gomp/$(BENCH_NAME)" 2>> $(BENCH_DIR)/log/$(NAME).compile; \
	$(CC_GOMP) $(COMMON_FLAGS) $(BENCH_FLAGS) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/gomp/$(BENCH_NAME) 2>> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo ""

compileIOMP: $(BENCH_DIR)/build $(BENCH_DIR)/log $(BENCH_DIR)/build/iomp
ifeq ($(CPP_FLAG),YES)
	$(eval CC_IOMP = $(CXX_IOMP))
endif
	echo "Compiling" $(BENCH_NAME); \
	echo "\n---------------------------------------------------------" >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	date >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo "$(CC_IOMP) $(COMMON_FLAGS) $(BENCH_FLAGS) -L$(LIBRARY_PATH) -I$(INCLUDE_PATH) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/iomp/$(BENCH_NAME)" 2>> $(BENCH_DIR)/log/$(NAME).compile; \
	$(CC_IOMP) $(COMMON_FLAGS) $(BENCH_FLAGS) -L$(LIBRARY_PATH) -I$(INCLUDE_PATH) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/iomp/$(BENCH_NAME) 2>> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo ""

compileMTSP: $(BENCH_DIR)/build $(BENCH_DIR)/log $(BENCH_DIR)/build/mtsp
ifeq ($(CPP_FLAG),YES)
	$(eval CC_IOMP = $(CXX_IOMP))
endif
	echo "Compiling" $(BENCH_NAME); \
	echo "\n---------------------------------------------------------" >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	date >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo "$(CC_IOMP) $(COMMON_FLAGS) $(BENCH_FLAGS) -L$(LD_PRELOAD) -L$(LIBRARY_PATH) -I$(INCLUDE_PATH) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/mtsp/$(BENCH_NAME)" 2>> $(BENCH_DIR)/log/$(NAME).compile; \
	$(CC_IOMP) $(COMMON_FLAGS) $(BENCH_FLAGS) -L$(LD_PRELOAD) -L$(LIBRARY_PATH) -I$(INCLUDE_PATH) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/mtsp/$(BENCH_NAME) 2>> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo ""

compileTIOGA: $(BENCH_DIR)/build $(BENCH_DIR)/log $(BENCH_DIR)/build/tioga
ifeq ($(CPP_FLAG),YES)
	$(eval CC_IOMP = $(CXX_IOMP))
endif
	echo "Compiling" $(BENCH_NAME); \
	echo "\n---------------------------------------------------------" >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	date >> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo "$(CC_IOMP) $(COMMON_FLAGS) $(BENCH_FLAGS) -L$(LD_PRELOAD) -L$(LIBRARY_PATH) -I$(INCLUDE_PATH) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/mtsp/$(BENCH_NAME)" 2>> $(BENCH_DIR)/log/$(NAME).compile; \
	$(CC_IOMP) $(COMMON_FLAGS) $(BENCH_FLAGS) -L$(LD_PRELOAD) -L$(LIBRARY_PATH) -I$(INCLUDE_PATH) $(AUX_SRC) $(SRC_OBJS) -o $(BENCH_DIR)/build/mtsp/$(BENCH_NAME) 2>> $(BENCH_DIR)/log/$(BENCH_NAME).compile; \
	echo ""

run: $(BENCH_DIR)/output
ifeq ($(OMP_LIB),gomp)
	make runGOMP
endif
ifeq ($(OMP_LIB),iomp)
	make runIOMP
endif
ifeq ($(OMP_LIB),mtsp)
	make runMTSP
endif
ifeq ($(OMP_LIB),tioga)
	make runTIOGA
endif
ifeq ($(OMP_LIB),none)
	make runSimple
endif
ifeq ($(OMP_LIB),seq)
	make runSEQ
endif

runSimple:
ifneq ($(INPUT),)
	$(eval INPUT_FLAGS = $(INPUT))
endif
	cd $(BENCH_DIR)/build;\
	echo "Running" $(BENCH_NAME); \
	echo "ulimit -t 300; ./$(BENCH_NAME) $(INPUT_FLAGS)" >> ../log/$(BENCH_NAME).execute; \
	ulimit -t 300; ./$(BENCH_NAME) $(INPUT_FLAGS) > ../log/$(BENCH_NAME).tmp; \
	cat ../log/$(BENCH_NAME).tmp; \
	cat ../log/$(BENCH_NAME).tmp >> ../log/$(BENCH_NAME).execute; \
	rm ../log/$(BENCH_NAME).tmp; \
	echo "\n"


runSEQ: $(BENCH_DIR)/output $(BENCH_DIR)/build/seq/$(BENCH_NAME)
ifneq ($(INPUT),)
	$(eval INPUT_FLAGS = $(INPUT))
endif
ifeq ($(OMP_LIB),seq)
	$(eval INPUT_FLAGS = $(INPUT_FLAGS_SEQUENTIAL))
endif
	cd $(BENCH_DIR)/build/seq;\
	echo "Running" $(BENCH_NAME); \
	echo "ulimit -t 3000; ./$(BENCH_NAME) $(INPUT_FLAGS)" >> ../../log/$(BENCH_NAME).execute; \
	ulimit -t 3000; ./$(BENCH_NAME) $(INPUT_FLAGS) > ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp >> ../../log/$(BENCH_NAME).execute; \
	rm ../../log/$(BENCH_NAME).tmp; \
	echo "\n"

runGOMP: $(BENCH_DIR)/output $(BENCH_DIR)/build/gomp $(BENCH_DIR)/build/gomp/$(BENCH_NAME)
ifneq ($(INPUT),)
	$(eval INPUT_FLAGS = $(INPUT))
endif
ifeq ($(OMP_LIB),seq)
	$(eval INPUT_FLAGS = $(INPUT_FLAGS_SEQUENTIAL))
endif
ifneq ($(TIME_LIMIT),)
	$(eval LIMIT_FLAG = ulimit -t $(TIME_LIMIT); )
	$(eval RUN_STRING = $(LIMIT_FLAG)./$(BENCH_NAME) $(INPUT_FLAGS))
else
	$(eval RUN_STRING = ./$(BENCH_NAME) $(INPUT_FLAGS))
endif
	cd $(BENCH_DIR)/build/gomp;\
	echo "Running" $(BENCH_NAME); \
	echo "$(RUN_STRING)" >> ../../log/$(BENCH_NAME).execute;\
	$(RUN_STRING) > ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp >> ../../log/$(BENCH_NAME).execute; \
	rm ../../log/$(BENCH_NAME).tmp; \
	echo "\n"

runIOMP: $(BENCH_DIR)/output $(BENCH_DIR)/build/iomp/$(BENCH_NAME)
ifneq ($(INPUT),)
	$(eval INPUT_FLAGS = $(INPUT))
endif
ifneq ($(TIME_LIMIT),)
	$(eval LIMIT_FLAG = ulimit -t $(TIME_LIMIT); )
	$(eval RUN_STRING = $(LIMIT_FLAG)LD_PRELOAD= LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) ./$(BENCH_NAME) $(INPUT_FLAGS))
else
	$(eval RUN_STRING = LD_PRELOAD= LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) ./$(BENCH_NAME) $(INPUT_FLAGS))
endif
	echo "OI";\
	cd $(BENCH_DIR)/build/iomp;\
	echo "Running" $(BENCH_NAME); \
	echo "$(RUN_STRING)"; \
	echo "$(RUN_STRING)" >> ../../log/$(BENCH_NAME).execute; \
	$(RUN_STRING) > ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp >> ../../log/$(BENCH_NAME).execute; \
	rm ../../log/$(BENCH_NAME).tmp; \
	echo "\n"

runMTSP: $(BENCH_DIR)/output $(BENCH_DIR)/build/mtsp/$(BENCH_NAME)
ifneq ($(INPUT),)
	#echo "ENTROU"
	$(eval INPUT_FLAGS = $(INPUT))
endif
ifneq ($(TIME_LIMIT),)
	$(eval LIMIT_FLAG = ulimit -t $(TIME_LIMIT); )
	$(eval RUN_STRING = $(LIMIT_FLAG)LD_PRELOAD=$(LD_PRELOAD) LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) ./$(BENCH_NAME) $(INPUT_FLAGS))
else
	$(eval RUN_STRING = LD_PRELOAD=$(LD_PRELOAD) LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) ./$(BENCH_NAME) $(INPUT_FLAGS))
endif
	#echo $(INPUT_FLAGS)
	cd $(BENCH_DIR)/build/mtsp; \
	echo "Running" $(BENCH_NAME); \
	echo "$(RUN_STRING)" >> ../../log/$(BENCH_NAME).execute; \
	$(RUN_STRING) > ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp >> ../../log/$(BENCH_NAME).execute; \
	rm ../../log/$(BENCH_NAME).tmp; \
	echo "\n"

runTIOGA: $(BENCH_DIR)/output $(BENCH_DIR)/build/tioga/$(BENCH_NAME)
ifneq ($(INPUT),)
	#echo "ENTROU"
	$(eval INPUT_FLAGS = $(INPUT))
endif
	#echo $(INPUT_FLAGS)
	cd $(BENCH_DIR)/build/tioga;\
	echo "Running" $(BENCH_NAME); \
	echo "ulimit -t 3000; MTSP_BRIDGE_MODE=1 LD_PRELOAD=$(LD_PRELOAD) LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) ./$(BENCH_NAME) $(INPUT_FLAGS)" >> ../../log/$(BENCH_NAME).execute; \
	ulimit -t 3000; MTSP_BRIDGE_MODE=1 LD_PRELOAD=$(LD_PRELOAD) LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) ./$(BENCH_NAME) $(INPUT_FLAGS) > ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp; \
	cat ../../log/$(BENCH_NAME).tmp >> ../../log/$(BENCH_NAME).execute; \
	rm ../../log/$(BENCH_NAME).tmp; \
	echo "\n"

runmali: $(BENCH_DIR)/build/$(BENCH_NAME)
ifneq ($(INPUT),)
	#echo "ENTROU"
	$(eval INPUT_FLAGS = $(INPUT))
endif
	cd $(BENCH_DIR)/build;\
	echo "Copying files to device"; \
	adb shell "mkdir -p /data/local/tmp/$(BENCH_DIR)"; \
	adb push ../build /data/local/tmp/$(BENCH_DIR)/build; \
	adb push ../input /data/local/tmp/$(BENCH_DIR)/input; \
	echo "\nRunning" $(BENCH_NAME); \
	echo "./$(BENCH_NAME) $(INPUT_FLAGS)" >> ../log/$(BENCH_NAME).execute; \
	adb shell "cd /data/local/tmp/$(BENCH_DIR)/build; ./$(BENCH_NAME) $(INPUT_FLAGS) > $(BENCH_NAME).tmp"; \
	adb pull "/data/local/tmp/$(BENCH_DIR)/build/$(BENCH_NAME).tmp" ../log/$(BENCH_NAME).tmp; \
	cat ../log/$(BENCH_NAME).tmp; \
	cat ../log/$(BENCH_NAME).tmp >> ../log/$(BENCH_NAME).execute; \
	rm ../log/$(BENCH_NAME).tmp; \
	echo "\n"
