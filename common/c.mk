COMMON_FLAGS=-fopenmp
#INSERT PATH TO FOLDER WHERE LIBOMP.SO IS LOCATED
LIBRARY_PATH=/home/linaro/Joaoland/openmp/runtime/build/src
#INSERT PATH TO FOLDER WHERE LIBOMP.SO IS LOCATED
LD_LIBRARY_PATH=/home/linaro/Joaoland/openmp/runtime/build/src:/home/linaro/Lukensville/aux_integration_test/
#LD_LIBRARY_PATH=/home/linaro/Joaoland/openmp/runtime/build/src
#INSERT PATH TO FOLDER WHERE OMP.H IS LOCATED
INCLUDE_PATH=/home/linaro/Joaoland/openmp/runtime/build/src
#INSERT DIRECT PATH TO MTSP.SO
LD_PRELOAD=/home/linaro/Lukensville/aux_integration_test/mtsp.so

INPUT_FOLDER=/home/linaro/Joaoland/input/Unibench-Input

CC=gcc-5
#C COMPILER FOR GOMP
CXX=g++-5
CC_GOMP=gcc-5
#CPP COMPILER FOR GOMP
CXX_GOMP=g++-5
#C COMPIELR FOR IOMP AND MTSP
CC_IOMP=/home/linaro/Joaoland/Debug+Asserts/bin/clang
#CPP COMPILER FOR IOMP AND MTSP
CXX_IOMP=/home/linaro/Joaoland/Debug+Asserts/bin/clang++

